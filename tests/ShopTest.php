<?php
include 'Shop.php';
class ShopTest extends \PHPUnit\Framework\TestCase {
    private $shop;

    protected function setUp(): void
    {
        $this->shop = new Shop();
    }

    public function testMostCostFurniture() {
        $furniture = $this->shop->mostCostFurniture('2000');
        $this->assertEquals(2, $furniture);
    }

    /**
     * @dataProvider addDataProvider
     */
    public function testCountFurnitureByColor($color, $expected){

        $this->assertEquals($expected, $this->shop->countFurnitureByColor($color));
    }

    public function addDataProvider(){
        return [
            ['gorih', 3],
            ['vishnya', 1],
        ];
    }

    protected function tearDown(): void
    {
        unset($this->shop);
    }
}