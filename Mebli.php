<?php

class Mebli
{
    private $name;
    private $color;
    private $cost;

    /**
     * Mebli constructor.
     * @param $name
     * @param $color
     * @param $cost
     */
    public function __construct($name, $color, $cost)
    {
        $this->name = $name;
        $this->color = $color;
        $this->cost = $cost;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getColor()
    {
        return $this->color;
    }

    public function setColor($color)
    {
        $this->color = $color;
    }

    public function getCost()
    {
        return $this->cost;
    }

    public function setCost($cost)
    {
        $this->cost = $cost;
    }
    }