<?php

include 'Mebli.php';
class Shop
{
    private $mebli;

    public function __constructor() {
        $this->mebli = array (
            new Mebli('Sonata', 'gorih', '2000'),
            new Mebli('Cartd', 'gorih', '2000'),
            new Mebli('Sonata', 'vishnya', '1500'),
            new Mebli('Carmen', 'gorih','1000'),
        );

    }

    public function mostCostFurniture($cost)
    {
        foreach ($this->mebli as $furniture) {
            if ($furniture->getCost() == $cost)
                return $furniture->getName();
        }
        return null;
    }

    public function countFurnitureByColor($color) {
        $count = 0;
        foreach ($this->mebli as $furniture){
            if ($furniture->getColor() == $color)
                $count++;
        }
        return $count;
    }
}
?>